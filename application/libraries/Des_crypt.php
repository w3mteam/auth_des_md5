<?php

/**
 * Library contains DES Algorithm implementaion 
 *  Powered BY OPADA-Eng .. Mars 2015
 */
class Des_crypt{
	/*
	 * $input: Data to be encrypted
	 * $key	 : encryption key
	 * $mode : algorithm mode such as: "ecb", "cbc", "cfb", "ofb"
	 */
	function encrypt($input,$key,$mode,$iv='') 
	{
		//Gets the block size of the specified cipher 
	    $size = mcrypt_get_block_size('des', $mode); 
		
	    $input = $this->pkcs5_pad($input, $size); 
	    
	    // Opens the module of the algorithm and the mode to be used
	    $td = mcrypt_module_open('des', '', $mode, ''); 
		if($iv == ''){
			//Creates an initialization vector (IV) from a random source
	    	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND); 
		}
		
		//This function initializes all buffers needed for encryption
	    mcrypt_generic_init($td, $key, $iv); 
		//This function encrypts data
	    $data = mcrypt_generic($td, $input);
		//This function deinitializes an encryption module 
	    mcrypt_generic_deinit($td); 
		//Closes the mcrypt module
	    mcrypt_module_close($td); 
		// encode crypted data
	    $data = base64_encode($data); 
	    return array('data'=>$data,'iv'=>$iv); 
	} 
	/*
	 * $input: Data to be decrypted
	 * $key	 : encryption key
	 * $iv	 : vector Used for the initialization in CBC, CFB, OFB modes , 
	 * 			it's value come from encryption function output
	 * $mode : algorithm mode such as: "ecb", "cbc", "cfb", "ofb"
	 */
	public function DES_verify($password,$hashed_password_with_iv)
	{
		$ci = &get_instance();
		$hashed_password = $hashed_password_with_iv->password;
		$mode = $ci->config->item('DES_mode', 'ion_auth');
		$key  = $ci->config->item('DES_key', 'ion_auth');
		$entered_password = $this->encrypt($password,$key, $mode,base64_decode($hashed_password_with_iv->iv));
		
		if (md5($entered_password['data']) == $hashed_password){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	function decrypt($input,$key,$iv,$mode)
	{
		$data = base64_decode($input);
		
		$result = mcrypt_decrypt('des',$key,$data,$mode,$iv);
		
	    $result = $this->pkcs5_unpad($result);
		
		return $result;
		 
		
	}
	// helper functions
	function pkcs5_pad ($text, $blocksize) 
	{ 
	    $pad = $blocksize - (strlen($text) % $blocksize); 
	    return $text . str_repeat(chr($pad), $pad); 
	} 
	// helper functions
	function pkcs5_unpad($text) 
	{ 
	    $pad = ord($text{strlen($text)-1}); 
	    if ($pad > strlen($text)) return false; 
	    if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false; 
	    return substr($text, 0, -1 * $pad); 
	}
}
