<?php
/**
 * 
 */
class Service extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
	}
	public function index()
	{
		$data['message']= '';
		$data['view_page']='uploader';
		$this->load->view('cpanel/cp_view_controller', $data);
	}
	private function config_upload()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5000';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';

		$this->load->library('upload', $config);
	}
	
	
	public function upload()
	{
		if($this->ion_auth->logged_in()){
			if($this->input->post('submit')){
				$this->config_upload();
				if ( $this->upload->do_upload('image'))
				{
					$file_data = $this->upload->data();
					$this->data['message']= 'Upload successed'.'<br>';		
				}
				else
				{
					$this->data['message']= $this->upload->display_errors().'<br>';
				}
				$this->data['view_page']='uploader';
				$this->load->view('cpanel/cp_view_controller', $this->data);
			}
		}
		else {
			$this->data['message']= 'Permission Denied';
			$this->data['view_page']='uploader';
			$this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
}
