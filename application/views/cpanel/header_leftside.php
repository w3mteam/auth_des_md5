
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Auth Des Md5</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/style-cpanel.css" rel="stylesheet" />
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>css/sb-admin.css" rel="stylesheet">
    <!-- Page Specific CSS -->
    <script src="<?php echo base_url();?>js/jquery-2.1.0.min.js" type="text/javascript" ></script>
    <script src="<?php echo base_url();?>js/bootstrap.js"></script>
  </head>
  <body>
    <div id="wrapper">
      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>index.php/auth/index">Auth Des Md5</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li <?php if(uri_string()=='auth/index'){?>class="active"><?php } else {?> > <?php }?> <a href="<?php echo base_url();?>index.php/account/index"><img src="<?php echo base_url();?>images/show_user.png" />Manage Users</a></li>
            <li <?php if(uri_string()=='auth/create_user'){?>class="active"><?php } else {?> > <?php }?> <a href="<?php echo base_url();?>index.php/service"><img src="<?php echo base_url();?>images/mypc_add.png" />Uploader</a></li>
         	
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
           		
            	<?php $user_id=$this->session->userdata('user_id');
                      $username=$this->session->userdata('username');?>
                <li><?php echo  anchor("auth/index/$user_id",$username); ?></li>
            	 <li><?php echo  anchor("account/logout",'logout'); ?></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

     
    </div><!-- /#wrapper -->

    
