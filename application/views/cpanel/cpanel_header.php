
﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">
    <link id="cssLink" href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
    <link id="cssLink" href="<?php echo base_url(); ?>css/style-cpanel.css" rel="stylesheet" media="screen">
    
    <script src="<?php echo base_url(); ?>js/jquery-1.8.3.min.js"></script> 
</head>
    <body>
        <header class="navbar color_header navbar-inverse navbar-fixed-top navbar-gray ">
            <div class="container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse_1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php echo  anchor("account",'لوحة التحكم','class="navbar-brand"'); ?>
                <div class="nav-collapse nav-collapse_1 collapse">
                    <ul class="nav navbar-nav">
                    	<?php $user_id=$this->session->userdata('user_id');
                    		$username=$this->session->userdata('username');?>
                        <li><?php echo  anchor("account/index/$user_id",$username); ?></li>
                        <li><?php echo  anchor("account/logout",'logout'); ?></li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
            
        </header>