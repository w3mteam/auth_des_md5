
<div class="col-md-6 col-sm-6 col-xs-6">
                    <br/>
                    
                    <div class="panel panel1">
                        <div class="panel-heading">
                            <p> <img src="<?php echo base_url(); ?>images/change_pass.png"  />تغير كلمة السر</p>
                        </div>
                        <div class="panel-body">
                            <h1><?php echo lang('change_password_heading');?></h1>

							<div id="infoMessage"><?php echo $message;?></div>
							
							<?php echo form_open("account/change_password");?>
							
							      <p>
							            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
							            <?php echo form_password($old_password,'','class="form-control font_input"  placeholder="كلمة السر القديمة" required=""');?>
							      </p>
							
							      <p>
							            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
							            <?php echo form_password($new_password,'','class="form-control font_input" placeholder="كلمة السر الجديدة" required=""');?>
							      </p>
							
							      <p>
							            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
							            <?php echo form_input($new_password_confirm,'','class="form-control font_input" placeholder="تأكيد كلمة السر الجديدة" required=""');?>
							      </p>
							
							      <?php echo form_input($user_id);?>
							      <p><?php echo form_submit('submit', lang('change_password_submit_btn'),'class="btn btn-lg btn-default btn-block"');?></p>
							
							<?php echo form_close();?>
                        </div>
                    </div>
                
            </div>