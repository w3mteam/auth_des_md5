<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Paper Stack</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
</head>
<body>
<div class="container">
	<section id="content">
		<?php 
		
			$first_name['id'] = 'username';
			$first_name['required']="";
			$first_name['placeholder'] = "First Name";
			$first_name['onclick']="this.value ='';";
			
			$last_name['id'] = 'username';
			$last_name['required']="";
			$last_name['placeholder'] = "Last Name";
			$last_name['onclick']="this.value ='';";
			
			$email['id'] = 'username';
			$email['type'] = 'email';
			$email['required']="";
			$email['placeholder'] = "name@mail-serv.com";
			$email['onclick']="this.value ='';";
			
			
			$password['id'] = 'password';
			$password['required']="";
			$password['placeholder'] = "Password";
			$password['onclick']="this.value ='';";
			
			$password_confirm['id'] = 'password';
			$password_confirm['required']="";
			$password_confirm['placeholder'] = "Confirm Password";
			$password_confirm['onclick']="this.value ='';";
			
			$phone['id'] = 'username';
			$phone['required']="";
			$phone['placeholder'] = "Phone";
			$phone['onclick']="this.value ='';";
			
			
		?>
		<?php echo form_open("account/create_user");?>
			<h1><?php echo 'Signup';?></h1>
			<div>
    			 <?php echo form_input($first_name);?>
			</div>
			<div>
    			 <?php echo form_input($last_name);?>
			</div>
			<div>
    			 <?php echo form_input($email);?>
			</div>
			<div>
				<?php echo form_input($password);?>
			</div>
			<div>
				<?php echo form_input($password_confirm);?>
			</div>
			<div>
    			 <?php echo form_input($phone);?>
			</div>
			
			
			<div>
				
				<?php echo form_submit('submit', 'Register');?>
				<a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
				<?php echo anchor('account/login','Login');?>
			</div>
		<?php echo form_close();?><!-- form -->
		<div class="button">
			<div id="infoMessage" style="color: red;"><?php echo $message;?></div>
		</div><!-- button -->
	</section><!-- content -->
	
</div><!-- container -->
</body>
</html>			     