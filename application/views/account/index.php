﻿

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/show_user.png"  />عرض المستخدمين</p>
            </div>
		<div class="panel-body">
            	<h1><?php echo lang('index_heading');?></h1>
				<p><?php echo lang('index_subheading');?></p>
				<div id="infoMessage"> <p><?php echo $message;?></p></div>
				
              <div class="table-responsive">
                    <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
                        </thead>
						
						<?php foreach ($users as $user):?>
						<tbody>
							<tr class="gradeX">
								<td><?php echo $user->first_name;?></td>
								<td><?php echo $user->last_name;?></td>
								<td><?php echo $user->email;?></td>
								<td><?php echo ($user->active) ? anchor("account/deactivate/".$user->id, lang('index_active_link')) : anchor("account/activate/". $user->id, lang('index_inactive_link'));?></td>
								<td><?php echo anchor("account/edit_user/".$user->id, lang('edit')) ;?></td>
							</tr>
						</tbody>
						<?php endforeach;?>
					</table>
				<p><?php echo anchor('account/create_user', lang('index_create_user_link'))?>  <?php //echo anchor('account/create_group', lang('index_create_group_link'))?></p>
                <!-- On rows -->
                </div>
            </div>
	</div>
</div>