


<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/delete_user.png"  /> حذف مستخدم</p>
            </div>
		 <div class="panel-body">
            	<h1><?php echo lang('deactivate_heading');?></h1>
				<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
				<?php echo form_open("account/deactivate/".$user->id);?>
				  <div class="radio">
	                                  <label>
	                                    <input type="radio" name="confirm" value="no" checked="checked">
	                                    <p>لا</p>
	                                  </label>
	                             </div>
	                             <div class="radio">
	                                  <label>
	                                    <input type="radio" name="confirm" value="yes" >
	                                    <p>نعم</p>
	                                  </label>
	                             </div>
				
				  <?php echo form_hidden($csrf); ?>
				  <?php echo form_hidden(array('id'=>$user->id)); ?>
				
				  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),'class="btn btn-lg btn-default btn-block" ');?></p>
				
				<?php echo form_close();?>
                   
            </div>
	</div>
</div>