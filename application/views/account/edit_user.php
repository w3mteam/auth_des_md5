

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> 
                	<?php echo $title;?></p>
            </div>
		  <div class="panel-body">
				<p><?php echo lang('edit_user_subheading');?></p>
				<div id="infoMessage"><?php echo $message;?></div>
							<?php  
							 echo form_open("account/edit_user/$user->id");
							 ?>
				      <p>
				            <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
				            <?php echo form_input($first_name,'','class="form-control font_input" placeholder="الاسم الأول" required=""');?>
				      </p>
				
				      <p>
				            <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
				            <?php echo form_input($last_name,'','class="form-control font_input" placeholder="الاسم الأخير" required=""');?>
				      </p>
				
				      
				
				      <p>
				            <?php echo lang('edit_user_phone_label', 'phone');?> <br />
				            <?php echo form_input($phone,'','class="form-control font_input" placeholder="رقم الهاتف"  required=""');?>
				      </p>
				      
					      <p>
					            <?php echo lang('edit_user_password_label', 'password').'(Optional)';?> <br />
					            <?php echo form_input($password,'','class="form-control font_input" placeholder="Password" ');?>
						  </p>
						
						  <p>
					            <?php echo lang('edit_user_password_confirm_label', 'password_confirm').'(Optional)';?><br />
					            <?php echo form_input($password_confirm,'','class="form-control font_input" placeholder="Confirm Password"  ');?>
						  </p>
					  
					  
					
					<h3><?php echo lang('edit_user_groups_heading');?></h3>
					<?php foreach ($groups as $group):?>
					<label class="checkbox">
					<?php
						$gID=$group['id'];
						$checked = null;
						$item = null;
						foreach($currentGroups as $grp) {
							if ($gID == $grp->id) {
								$checked= ' checked="checked"';
							break;
							}
						}
					?>
					<input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
					<?php echo $group['name'];?>
					</label>
					<?php endforeach?>
					
				      <?php echo form_hidden('id', $user->id);?>
				      <?php echo form_hidden($csrf); ?>
				
				      <p><?php echo form_submit('submit', 'Save','class="btn btn-lg btn-default btn-block"');?></p>
				
				<?php echo form_close();?>
              
            </div>
	</div>
</div>