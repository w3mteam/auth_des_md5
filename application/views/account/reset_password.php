
﻿<!DOCTYPE html>
<html>
<head>
    <link id="cssLink" href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
    <link id="cssLink" href="<?php echo base_url(); ?>css/style-cpanel.css" rel="stylesheet" media="screen">
</head>
    <body>
        <header class="navbar color_header navbar-inverse navbar-fixed-top navbar-gray ">
            
        </header>
        <div class="row">
           <div class="col-md-4 col-xs-4 col-sm-4"></div>
            <div class="col-md-4 col-xs-4 col-sm-4">
                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p><?php echo lang('reset_password_heading');?></p>
                    </div>
                    <div class="panel-body">
                    	<h1><?php echo lang('reset_password_heading');?></h1>
						<div id="infoMessage"><?php echo $message;?></div>
						
						<?php echo form_open('account/reset_password/' . $code);?>
						
							<p>
								<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
								<?php echo form_input($new_password,'','class="form-control"  required=""');?>
							</p>
						
							<p>
								<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
								<?php echo form_input($new_password_confirm ,'','class="form-control" required=""');?>
							</p>
						
							<?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>
						
							<p><?php echo form_submit('submit', lang('reset_password_submit_btn'));?></p>
						
						<?php echo form_close();?>
						
						<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

                    </div>
                </div>
                
                
            </div>
            <div class="col-md-4 col-xs-4 col-sm-4"></div>
        </div>
       
        <script src="<?php echo base_url(); ?>js/jquery1.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
        
        <!-- Custom JavaScript for the Menu Toggle -->
   
    </body>
</html>
