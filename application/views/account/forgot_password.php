

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Paper Stack</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />

</head>
<body>
<div class="container">
	<section id="content">
		
		<?php echo form_open("account/forgot_password");?>
		<h1><?php echo lang('forgot_password_heading');?></h1>
		<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
		  <p>
	      	<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
	      	<?php echo form_input($email,'','class="form-control" placeholder="البريد الإلكتروني" required=""');?>
	      </p>
	      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn btn-lg btn-default btn-block"');?></p>
	
		<?php echo form_close();?>
		<div id="infoMessage"><?php echo $message;?></div><br />
	</section><!-- content -->
	
</div><!-- container -->
</body>
</html>