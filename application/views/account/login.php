<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Paper Stack</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
</head>
<body>
<div class="container">
	<section id="content">
		<?php 
			$identity['id'] = 'username';
			$identity['required']="";
			$identity['placeholder'] = "Username";
			$identity['onclick']="this.value ='';";
			
			$password['id'] = 'password';
			$password['required']="";
			$password['placeholder'] = "Password";
			$password['onclick']="this.value ='';";
		?>
		<?php echo form_open("account/login");?>
			<h1><?php echo lang('login_heading');?></h1>
			<div>
    			 <?php echo form_input($identity);?>
			</div>
			<div>
				<?php echo form_input($password);?>
			</div>
			<div>
				<?php echo lang('login_remember_label', 'remember');?>
    			<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
			</div>
			<div>
				
				<?php echo form_submit('submit', lang('login_submit_btn'));?>
				<a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
				<?php echo anchor('account/create_user','Register');?>
			</div>
		<?php echo form_close();?><!-- form -->
		<div class="button">
			<div id="infoMessage" style="color: red;"><?php echo $message;?></div>
		</div><!-- button -->
	</section><!-- content -->
	
</div><!-- container -->
</body>
</html>